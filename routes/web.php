<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/templates/login/login', function () {
    return view('templates.login.login');
});

Route::get('/templates/extranet/extranet', function () {
    return view('templates.extranet.extranet');
});
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'Login@index');
Route::group(['prefix'=>'login'], function () {
    Route::post('/valida_dni', 'Login@ValidaDNI')->name('valida_dni');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
