<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    public $table = 'usuarios';
    public $timestamps = false;
    protected $fillable = [
        'dni',
    ];
}
