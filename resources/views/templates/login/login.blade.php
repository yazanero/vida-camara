@extends('layouts.login')
@section('content')

<div class="container flex__row login">
    <div class="flex__column--1 login__content">
        <img src="{{URL::asset('/images/logo.png')}}" alt="logo">
        <div class="login__title">
            Regístrate
            {{-- ¡Gracias por registrarte! --}}
            {{-- Ingresa a tu cuenta --}}
        </div>

        {{-- <div class="login__revisa-correo">
            <div class="login__revisa-correo-text">
                Revisa tu correo e ingresa al <br>
                enlace que te hemos enviado.
            </div>
        </div> --}}
        <div class="login__content-input">
            <div class="login__user-text"><i class="icon-happy-face"></i> Marco P. Hurtado S. - 19820685</div> 
            <div class="login__input-text">
                Escribe el número de tu DNI
            </div>
            <input type="text" name="" id="" class="login__input input__text error" placeholder="Escribe el número de tu DNI">
            <div class="login__content-icon">
                {{-- <i class="icon-mostrar"></i> --}}
                <i class="icon-ocultar"></i>
                <div class="tooltip tooltip--right"><div class="tooltip__text">Ocultar</div></div>
            </div>
            {{-- <div class="alerta">
                <i class="icon-alert"></i>
                <div class="alerta__content-text">
                    <div class="alerta__text">Contraseña incorrecta</div>
                </div>
            </div> --}}
            {{-- <div class="alerta">
                <i class="icon-alert"></i>
                <div class="alerta__content-text">
                    <div class="alerta__text">DNI no registrado</div>
                    <br>
                    <div class="alerta__text">Llámanos al 604-2000 para <br> absolver tus dudas</div>
                </div>
            </div> --}}
        </div>
        <div class="login__content-btn">
            <div class="btn btn--default">Siguiente</div>
        </div>
        <div class="login__ingresa">
            <p class="login__ingresa-text">¿Ya tienes una cuenta?</p>
            <p class="login__ingresa-text">¿No tienes una cuenta?</p>
            <a href="#" class="link login__link">ingresa</a>
        </div>
    </div>
    <div class="flex__column--1 login__fondo" style="background-image: url({{URL::asset('/images/login-portada.png')}}) ">
    </div>
</div>

@stop
@section('scripts')
    
@stop