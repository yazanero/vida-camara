@extends('layouts.default')
@section('content')
    <div class="container extranet bg-main" style="padding-top:100px">
        <div class="flex__row separacion__row--30">
            <aside class="flex__column--mw boletas boletas-card separacion__column--30">
                <div class="boletas__content">
                    <div class="boletas__content-rombo">
                        <button class="btn btn--circle"><i class="icon-menor"></i></button>
                        <div class="boletas__content-form">
                            <div class="rombo">
                                <div class="text-boletas">Boletas</div>
                                <div class="text-years">2018</div>
                            </div>
                        </div>
                        <button class="btn btn--circle" disabled><i class="icon-mayor"></i></button>
                    </div>
                    <div class="boletas__content-btn">
                        <div class="btn btn--bordered"><i class="icon-download"></i>Descargar selección</div>
                        <div class="btn btn--bordered"><i class="icon-message"></i>Enviar selección</div>
                    </div>
                    <div class="boletas__content-recuerda">
                        <div class="text-recuerda">
                            Recuerda que puedes descargar varias boletas, enviarlas a tu correo o al correo de alguien de tu entera confianza.
                        </div>
                    </div>
                </div>
            </aside>
            <div class="flex__column--1 meses separacion__column--30">
                <ul class="meses__items flex__row">
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="content-checkbox">
                                <input type="checkbox" class="circle" name="" id="">
                                <div class="tooltip tooltip--top"><div class="tooltip__text">Seleccionar</div></div>
                            </div>
                            <div class="meses-text--abreviado">ENE</div>
                            <div class="meses-text--completo">Enero</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">FEB</div>
                            <div class="meses-text--completo">Febrero</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">MAR</div>
                            <div class="meses-text--completo">Marzo</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">ABR</div>
                            <div class="meses-text--completo">Abril</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">MAY</div>
                            <div class="meses-text--completo">Mayo</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">JUN</div>
                            <div class="meses-text--completo">Junio</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">JUL</div>
                            <div class="meses-text--completo">Julio</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">AGO</div>
                            <div class="meses-text--completo">Agosto</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">SEP</div>
                            <div class="meses-text--completo">Septiembre</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">OCT</div>
                            <div class="meses-text--completo">Octubre</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses-text--abreviado">NOV</div>
                            <div class="meses-text--completo">Noviembre</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                    <li class="meses__item flex__column">
                        <div class="meses-card">
                            <div class="circle"></div>
                            <div class="meses__nuevo">Nuevo</div>
                            <div class="content-btn">
                                <div class="btn btn--bordered ver-boleta">Ver boleta</div>
                            </div>
                            <div class="meses-text--abreviado">DIC</div>
                            <div class="meses-text--completo">Diciembre</div>
                        </div>
                        <div class="btn btn--bordered">Descargar</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    
@stop