<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('includes.head')
    </head>
    <body>
        @include('includes.header') 
        <main id="app">
            @yield('content')
        </main>

        @include('includes.footer')
        {{-- @routes --}}

        <script src="{{URL::asset('/js/app.js')}}?v=1"></script>
        <!--<script src="/js/app.js"></script>-->
        @yield('scripts')
    </body>
</html>
