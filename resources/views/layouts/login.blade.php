<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('includes.head')
    </head>
    <body>
        <main id="app">
            @yield('content')
        </main>
        <script src="{{URL::asset('/js/app.js')}}?v=1"></script>
        <!--<script src="/js/app.js"></script>-->
        @yield('scripts')
    </body>
</html>
