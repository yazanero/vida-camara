<header class="header">
    <div class="header__content container">
        <div class="header__content-logo">
            <img class="header__logo" src="{{URL::asset('/images/logo-header.png')}}" alt="logo">
        </div>
        <div class="header__content-avatar">
            <div class="header__content-text">Hola Jorge, te damos la bienvenida.</div>
            <div class="header__avatar">
                <img src="{{URL::asset('/images/avatar.png')}}" alt="">
            </div>
        </div>
    </div>
</header>
        
        
    