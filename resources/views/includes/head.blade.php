<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{{ asset('favicon.png') }}}">

<title>Mi Banco</title>

<link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}?v=1">
