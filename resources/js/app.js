
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vuex from 'vuex';
Vue.use(Vuex);
import refStore from './components/store';
const store = new Vuex.Store(refStore);
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import BootstrapVue from 'bootstrap-vue';f

Vue.use(BootstrapVue);

Vue.component('example-component', require('./components/ExampleComponent.vue'));
// Vue.component('login', require('./components/Login/General.vue'));
Vue.component('registro', require('./components/Registro/General.vue'));

const app = new Vue({
    el: '#app',
    store
});
