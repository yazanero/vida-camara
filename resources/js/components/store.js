export default {
    state: {
        paso:1,
        data:{
            dni:''
        }
    },
    getters: {
        getPaso(state) {
            return state.paso;
        }
    },
    mutations: {
        actualizapaso(state, payload) {
            state.paso = payload;
        }
    }
};